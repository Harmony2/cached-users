import sys
import requests
import csv
import os

CACHE_FILE = "./cache.csv" #Cache file path

class User:
    def __init__(self, username):
        try:
            self.email, self.website, self.hemisphere = self.__get_user_from_cache(username) #Searches user on cache
        except:
            self.email, self.website, self.hemisphere = self.__get_user_from_api(username) #Fetches user from API

    def __get_hemisphere(self, lat): #Infer hemisphere from latitude
        if lat > 0 : return "Northern"
        return "Southern"

    def __cache_user(self, username, email, website, hemisphere): #Caches user on csv file
        f = open(CACHE_FILE, "a+")
        writer = csv.writer(f)
        writer.writerow([username, email, website, hemisphere])
        f.close()
        return True

    def __get_user_from_cache(self, username): #Gets user from csv file
        f = open(CACHE_FILE, "r")
        reader = csv.reader(f)

        #This function would benefit from binary search implementation
        for row in reader:
            if row[0] == username: #If username in this row is the same username provided by the user
                email, website, hemisphere = row[1], row[2], row[3]
                f.close()
                print("Fetch from Cache")
                return email, website, hemisphere
        
        f.close()
        return False

    def __get_user_from_api(self, username):
        try:
            r = requests.get("https://jsonplaceholder.typicode.com/users?username=%s" % username) #Get request to API
        except Exception as e:
            print("Could not fetch data from API, nor find it in your cache, please, check your connection to the internet and try again!")
            sys.exit()

        try:
            email = r.json()[0]["email"]
            website = r.json()[0]["website"]
            lat = float(r.json()[0]["address"]["geo"]["lat"])
            hemisphere = self.__get_hemisphere(lat)

        except:
            print("Could not find any data about this user.")
            print("Please, check the username spelling and try again!")
            sys.exit()

        self.__cache_user(username, email, website, hemisphere)
        print("Fetch from API")
        return email, website, hemisphere


def reset_cache(): #Resets cache, useful if user thinks their cache is outdated
    try:
        os.remove(CACHE_FILE)
        return True
    except:
        return False


if __name__ == '__main__':
    try:
        username = sys.argv[1] #Get username from commandline 
    except:
        print("Please, insert an username and try again.")
        sys.exit()

    if len(sys.argv) >= 3:
        if sys.argv[2].lower() == "-r":
            reset_cache_option = input("Would you like to reset cache? (y/n)\n").lower()
            if reset_cache_option == "y":
                reset_cache()

    user = User(username)

print("username: %s" % username)
print("email: %s" % user.email)
print("website: %s" % user.website)
print("hemisphere: %s" % user.hemisphere)