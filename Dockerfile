FROM python:3.7-alpine

WORKDIR /app/

COPY /code/* /app/

RUN pip install -r /app/requirements.txt