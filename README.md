# Cached Users

A aplicação realiza requisições para a API disponibilizada (https://jsonplaceholder.typicode.com/users) afim de extrair informações a respeito de um usuário inserido na commandline durante a chamada do código. Após a primeira requisição à API, as informações de interesse obtidas são inseridas em cache. As informações dos usuários cujo cache já detém serão sempre requeridas de tal, evitando requisições desnecessárias à API.

# Utilização

Toda o código da aplicação está contido na pasta *_/code/_*

O script da aplicação está no arquivo *api_users.py*

Para utilizar a aplicação, há duas maneiras principais

## Python

Você pode usar o código clonando o repositório e instalando as dependências em seu ambiente python; Todas as dependências estão contidas em */code/requirements.txt* (há apenas uma dependência, a biblioteca *requests*).

    pip3 install -r requirements.txt 

Após a instalação, basta realizar a chamada da aplicação, passando os argumentos na commandline. Um exemplo extrair as informações do usuário cujo *username* é Bret:

    python3 /code/api_users.py Bret

## Docker

Após clonar este repositório, você pode buildar a imagem Docker em sua máquina utilizando a Docker Image disponibilizada.
A partir da pasta raiz deste repositório:

    sudo docker build -t chached_users:latest .

Após buildar a imagem, basta executá-la:

    sudo docker run --name cached chached_users:latest python /app/api_users.py Bret

Você também pode executar a em modo interativo, e executar o script de dentro do container gerado:

    sudo docker run --name cached -ti chached_users:latest /bin/ash

    python /app/api_users.py Bret

# Resetando o cache

Numa situação de mundo real, o usuário poderia se encontrar numa situação onde seu cache está desatualizado. Para facilitar sua atualização, inserí uma opção que deleta o cache anterior. Para utilizá-la, basta adicionar _-r_ após o comando, e a aplicação te perguntará se gostaria de resetar o cache.

    python api_users.py Bret -r

# Pipeline CI

Há uma pipeline para a aplicação; Esta pipeline é executada de acordo com o script contido no arquivo *.gitlab-ci.yml* e será executada mediante execução manual, ou automática quando um commit for aprovado para qualquer branch. A pipeline é capaz de:

* [x] Buildar a Docker Image contida no repositório, com os arquivos mais recentes naquela branch
* [x] Realizar um teste simples a partir do container gerada com tal docker image
* [x] SAST
* [x] Análise de qualidade de código
* [X] Análise de vulnerabilidade do container

Esta pipeline foi projeta visando possibilitar uma visão mais acurada da qualidade da aplicação como um todo, tornando mais fácil encontrar erros de vulnerabilidade, performance, e más práticas no código. Tais testes e scans tornam mais facilmente palpável a implementação de medidas e correções assertivas à aplicação, e sua execução automática facilita todo o este processo.